import datetime

from setuptools import setup, find_namespace_packages

NAMESPACE = 'mosaik_scenario_tools'
PACKAGE = 'scenario_tools'

TIMESTAMP = str(datetime.datetime.now().replace(microsecond=0).isoformat()).\
    replace('-', '').replace('T', '').replace(':', '')

setup(
    author='Bengt Lüers',
    author_email='bengt.lueers@gmail.com',
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: GNU Lesser General Public License v2 '
        '(LGPLv2)',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Topic :: Scientific/Engineering',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ],
    cmdclass={
    },
    description='Tools for building mosaik scenarios',
    entry_points={
    },
    include_package_data=True,
    install_requires=[
        'mosaik.API-SemVer>=2.4.2rc20190716091443',
        'mosaik.Core-SemVer>=2.5.2rc20190715231038',
        'pip>=20.3',
        'setuptools>=40.8.0',
        'wheel>=0.33.1',
    ],
    long_description=(
            open('README.md').read()
    ),
    long_description_content_type='text/markdown',
    maintainer='Bengt Lüers',
    maintainer_email='bengt.lueers@gmail.com',
    name='mosaik' + '.' + 'Scenario-Tools',
    packages=find_namespace_packages(include=[NAMESPACE + '.*']),
    package_dir={'': '.'},
    setup_requires=[
    ],
    tests_require=[
        'pytest',
    ],
    url='https://gitlab.com/offis.energy/mosaik/mosaik.scenario-tools',
    version='0.1.0' + 'rc' + TIMESTAMP,
    zip_safe=False,
)
