import mosaik
import pytest
from mosaik_simconfig.simconfig.sim_config import SimConfig

from mosaik_scenario_tools.scenario_tools.sink.sink_simulator import \
    SinkSimulator
from mosaik_scenario_tools.scenario_tools.source.source_simulator import \
    SourceSimulator


@pytest.fixture(name='world')
def world_fixture():
    sim_config = SimConfig()
    sim_config.add_in_process(simulator=SourceSimulator)
    sim_config.add_in_process(simulator=SinkSimulator)

    world = mosaik.scenario.World(sim_config=sim_config)

    yield world

    world.shutdown()
