from mosaik_scenario_tools.scenario_tools.connect.connect_one_to_many_module \
    import connect_one_to_many


def test_connect_one_to_many(world, source_model0, sink_model0, sink_model1):
    connect_one_to_many(
        source=source_model0,
        sinks={sink_model0, sink_model1},
        attribute_pairs='my_attribute',
        world=world,
    )

    world.run(until=1)
