import pytest

from mosaik_scenario_tools.scenario_tools.create.create_model_module import \
    create_model
from mosaik_scenario_tools.scenario_tools.sink.sink_model import SinkModel
from mosaik_scenario_tools.scenario_tools.source.source_model import \
    SourceModel
from mosaik_scenario_tools.scenario_tools.sink.sink_simulator import \
    SinkSimulator
from mosaik_scenario_tools.scenario_tools.source.source_simulator import \
    SourceSimulator


@pytest.fixture(name='source_model0')
def source_model0_fixture(world):
    source_model0 = create_model(
        world=world, simulator=SourceSimulator, model=SourceModel
    )

    return source_model0


@pytest.fixture(name='source_model1')
def source_model1_fixture(world):
    source_model1 = create_model(
        world=world, simulator=SourceSimulator, model=SourceModel
    )

    return source_model1


@pytest.fixture(name='sink_model0')
def sink_model0_fixture(world):
    sink_model0 = create_model(
        world=world, simulator=SinkSimulator, model=SinkModel
    )

    return sink_model0


@pytest.fixture(name='sink_model1')
def sink_model1_fixture(world):
    sink_model1 = create_model(
        world=world, simulator=SinkSimulator, model=SinkModel
    )

    return sink_model1
