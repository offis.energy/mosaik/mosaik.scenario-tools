from mosaik_scenario_tools.scenario_tools.connect.connect_many_to_one_module \
    import connect_many_to_one


def test_connect_many_to_one(world, source_model0, source_model1, sink_model0):
    connect_many_to_one(
        sources={source_model0, source_model1},
        sink=sink_model0,
        attribute_pairs='my_attribute',
        world=world,
    )

    world.run(until=1)
