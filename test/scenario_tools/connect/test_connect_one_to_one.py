from mosaik_scenario_tools.scenario_tools.connect.connect_one_to_one_module \
    import connect_one_to_one
from mosaik_scenario_tools.scenario_tools.sink.sink_model import SinkModel
from mosaik_scenario_tools.scenario_tools.sink.sink_simulator import \
    SinkSimulator
from mosaik_scenario_tools.scenario_tools.source.source_model import \
    SourceModel
from mosaik_scenario_tools.scenario_tools.source.source_simulator import \
    SourceSimulator


def test_connect_one_to_one(world):
    # Create mock data source
    source_simulator = world.start(sim_name=SourceSimulator.__name__)
    source_model_mock = \
        getattr(source_simulator, SourceModel.__name__)
    source_model = source_model_mock()

    # Create mock data sink
    sink_simulator = world.start(sim_name=SinkSimulator.__name__)
    sink_model_mock = \
        getattr(sink_simulator, SinkModel.__name__)
    sink_model = sink_model_mock()

    # Connect source to sink directly
    connect_one_to_one(
        world=world,
        source=source_model,
        sink=sink_model,
        attribute_pairs='my_attribute',
    )


def test_simulation(world):
    # Run simulation for a single time step
    world.run(until=1)
