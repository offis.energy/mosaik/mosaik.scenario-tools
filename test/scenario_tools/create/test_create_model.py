from mosaik.scenario import Entity
from mosaik.util import connect_many_to_one

from mosaik_scenario_tools.scenario_tools.connect.connect_one_to_one_module \
    import connect_one_to_one
from mosaik_scenario_tools.scenario_tools.create.create_model_module import \
    create_model
from mosaik_scenario_tools.scenario_tools.sink.sink_model import SinkModel
from mosaik_scenario_tools.scenario_tools.sink.sink_simulator import \
    SinkSimulator
from mosaik_scenario_tools.scenario_tools.source.source_model import \
    SourceModel
from mosaik_scenario_tools.scenario_tools.source.source_simulator import \
    SourceSimulator
from mosaik_scenario_tools.scenario_tools.connect.connect_one_to_many_module \
    import connect_one_to_many


def test_create_model_type_source(world):
    # Test
    source_model = create_model(
        world=world, simulator=SourceSimulator, model=SourceModel
    )

    # Assert
    assert source_model is not None
    assert isinstance(source_model, Entity)
    assert source_model.type is SourceModel.__name__


def test_create_model_type_sink(world):
    # Test
    sink_model = create_model(
        world=world, simulator=SinkSimulator, model=SinkModel
    )

    # Assert
    assert sink_model is not None
    assert isinstance(sink_model, Entity)
    assert sink_model.type is SinkModel.__name__


def test_create_one_model_each_connect_and_run(world):
    # Test
    source_model = create_model(
        world=world, simulator=SourceSimulator, model=SourceModel
    )
    sink_model = create_model(
        world=world, simulator=SinkSimulator, model=SinkModel
    )

    connect_one_to_one(
        source=source_model,
        sink=sink_model,
        world=world,
        attribute_pairs='my_attribute'
    )

    world.run(until=1)

    # Assert
    # If we get here without error, consider that a success
    assert True


def test_create_two_model_each_and_test_types(world):
    # Test
    source_model0 = create_model(
        world=world, simulator=SourceSimulator, model=SourceModel
    )

    # Assert
    assert source_model0 is not None
    assert isinstance(source_model0, Entity)
    assert source_model0.type is SourceModel.__name__

    source_model1 = create_model(
        world=world, simulator=SourceSimulator, model=SourceModel
    )

    # Assert
    assert source_model1 is not None
    assert isinstance(source_model1, Entity)
    assert source_model1.type is SourceModel.__name__

    sink_model0 = create_model(
        world=world, simulator=SinkSimulator, model=SinkModel
    )

    # Assert
    assert sink_model0 is not None
    assert isinstance(sink_model0, Entity)
    assert sink_model0.type is SinkModel.__name__

    sink_model1 = create_model(
        world=world, simulator=SinkSimulator, model=SinkModel
    )

    # Assert
    assert sink_model1 is not None
    assert isinstance(sink_model1, Entity)
    assert sink_model1.type is SinkModel.__name__


def test_create_two_and_one_model_each_connect_many_to_one(world):
    # Test
    source_model0 = create_model(
        world=world, simulator=SourceSimulator, model=SourceModel
    )

    source_model1 = create_model(
        world=world, simulator=SourceSimulator, model=SourceModel
    )

    sink_model0 = create_model(
        world=world, simulator=SinkSimulator, model=SinkModel
    )

    connect_many_to_one(
        src_set={source_model0, source_model1},
        dest=sink_model0,
        world=world,
    )


def test_create_one_and_two_model_each_connect_one_to_many(world):
    # Test
    source_model0 = create_model(
        world=world, simulator=SourceSimulator, model=SourceModel
    )

    sink_model0 = create_model(
        world=world, simulator=SinkSimulator, model=SinkModel
    )

    sink_model1 = create_model(
        world=world, simulator=SinkSimulator, model=SinkModel
    )

    connect_one_to_many(
        source=source_model0,
        sinks={sink_model0, sink_model1},
        world=world,
        attribute_pairs='my_attribute'
    )
