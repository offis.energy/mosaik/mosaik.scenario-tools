import pytest

from mosaik_scenario_tools.scenario_tools.source.source_model import SourceModel
from mosaik_scenario_tools.scenario_tools.source.source_simulator import \
    SourceSimulator


@pytest.fixture(name='source_simulator')
def source_simulator_fixture():
    simulator = SourceSimulator()
    return simulator


def test_source_simulator_once(source_simulator):
    model_list = source_simulator.create(
        num=1,
        model=SourceModel.__name__,
    )

    assert model_list is not None
    assert isinstance(model_list, list)
    assert model_list[0]['type'] == SourceModel.__name__


def test_source_simulator_twice(source_simulator):
    _ = source_simulator.create(
        num=1,
        model=SourceModel.__name__,
    )
    with pytest.raises(NotImplementedError):
        _ = source_simulator.create(
            num=1,
            model=SourceModel.__name__,
        )


def test_source_simulator_wrong_number(source_simulator):
    with pytest.raises(NotImplementedError):
        _ = source_simulator.create(
            num=2,
            model=SourceModel.__name__,
        )


def test_source_simulator_wrong_name(source_simulator):
    with pytest.raises(NotImplementedError):
        _ = source_simulator.create(
            num=1,
            model=SourceModel.__name__ + '_',
        )


def test_source_simulator_model_params(source_simulator):
    with pytest.raises(NotImplementedError):
        _ = source_simulator.create(
            num=1,
            model=SourceModel.__name__ + '_',
            model_params={'model_param_0': 1},
        )


def test_source_simulator_step(source_simulator):
    source_simulator.step(
        time=0,
        inputs={'source_model-0': {'my_attribute': 0}}
    )


def test_source_simulator_step_and_get_data(source_simulator):
    source_simulator.step(
        time=0,
        inputs={'source_model-0': {'my_attribute': 0}}
    )
    data = source_simulator.get_data(
        outputs={'source_model-0': {'my_attribute'}}
    )

    assert data is not None
    assert isinstance(data, dict)
    assert data == {}


def test_source_simulator_create_step_and_get_data(source_simulator):
    _ = source_simulator.create(
        num=1,
        model=SourceModel.__name__,
    )
    source_simulator.step(
        time=0,
        inputs={'SourceModel-0': {'my_attribute': 0}}
    )
    data = source_simulator.get_data(
        outputs={'SourceModel-0': {'my_attribute'}}
    )

    assert data is not None
    assert isinstance(data, dict)
    assert data != {}

    assert 'SourceModel-0' in data.keys()
