from mosaik_scenario_tools.scenario_tools.simulator.buffer_inputs.\
    input_buffering_meta import INPUT_BUFFERING_META
from mosaik_scenario_tools.scenario_tools.simulator.manage_inputs.\
    input_managing_scenario import input_managing_scenario
from mosaik_scenario_tools.scenario_tools.simulator.manage_inputs.\
    input_managing_simulator import InputManagingSimulator


def test_input_buffering_simulator():
    simulator = InputManagingSimulator(meta=INPUT_BUFFERING_META)

    inputs_first_step = {
        'x': 'bar',
        'clock_output_dto':
            {
                'ClockSimulator-0.ClockModel-0': {'phase': "LIMBO"}
              }
    }
    simulator.step(time=0, inputs=inputs_first_step)
    assert simulator.__inputs == inputs_first_step

    inputs_second_step = {
        'clock_output_dto':
            {
                'ClockSimulator-0.ClockModel-0': {'phase': "INGEST"}
              }
    }
    simulator.step(time=0, inputs=inputs_second_step)
    assert simulator.__inputs_future == inputs_second_step
    assert simulator.__inputs_current == inputs_first_step


def test_input_managing_scenario():
    input_managing_scenario()
