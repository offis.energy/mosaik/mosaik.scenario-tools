from mosaik_scenario_tools.scenario_tools.simulator.buffer_inputs.\
    input_buffering_meta import INPUT_BUFFERING_META
from mosaik_scenario_tools.scenario_tools.simulator.buffer_inputs.\
    input_buffering_scenario import input_buffering_scenario
from mosaik_scenario_tools.scenario_tools.simulator.buffer_inputs.\
    input_buffering_simulator import InputBufferingSimulator


def test_input_buffering_simulator():
    simulator = InputBufferingSimulator(meta=INPUT_BUFFERING_META)

    inputs = {}
    simulator.step(time=0, inputs=inputs)
    assert simulator.__inputs == {}

    inputs = {
        'x': 'foo',
    }
    simulator.step(time=0, inputs=inputs)
    assert simulator.__inputs == {
        'x': 'foo',
    }

    inputs = {
        'x': 'foo',
    }
    simulator.step(time=0, inputs=inputs)
    assert simulator.__inputs == {
        'x': 'foo',
    }

    inputs = {
        'y': 'bar',
    }
    simulator.step(time=0, inputs=inputs)
    assert simulator.__inputs == {
        'x': 'foo',
        'y': 'bar',
    }

    inputs = {
        'x': 'bar',
        'y': 'foo',
    }
    simulator.step(time=0, inputs=inputs)
    assert simulator.__inputs == {
        'x': 'bar',
        'y': 'foo',
    }


def test_input_buffering_scenario():
    input_buffering_scenario()
