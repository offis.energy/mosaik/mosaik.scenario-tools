import socket
from mosaik_scenario_tools.scenario_tools.port.find_free_port_module import \
    find_free_port


def test_find_free_port_type():
    port = find_free_port()

    assert isinstance(port, int)
    assert 1 < port < 65536


def test_find_free_port_bind():
    port = find_free_port()
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # Try to bind to the port
    sock.bind(("localhost", port))

    # If we bind without an error, the port was indeed free
    assert True
