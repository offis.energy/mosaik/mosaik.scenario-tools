from mosaik_scenario_tools.scenario_tools.model.get_first_source_key_module \
    import get_first_source_key


def test_get_first_source_key():
    first_source_key_expected = 'Source-0'
    inputs = {
        'my_attribute': {
            first_source_key_expected:
                False,
        }
    }

    first_source_key_actual = get_first_source_key(
        inputs=inputs,
        attribute='my_attribute',
    )

    assert first_source_key_actual == first_source_key_expected
