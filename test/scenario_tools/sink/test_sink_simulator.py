import pytest

from mosaik_scenario_tools.scenario_tools.sink.sink_model import SinkModel
from mosaik_scenario_tools.scenario_tools.sink.sink_simulator import \
    SinkSimulator


@pytest.fixture(name='sink_simulator')
def sink_simulator_fixture():
    simulator = SinkSimulator()
    return simulator


def test_sink_simulator_once(sink_simulator):
    model_list = sink_simulator.create(
        num=1,
        model=SinkModel.__name__,
    )

    assert model_list is not None
    assert isinstance(model_list, list)
    assert model_list[0]['type'] == SinkModel.__name__


def test_sink_simulator_twice(sink_simulator):
    _ = sink_simulator.create(
        num=1,
        model=SinkModel.__name__,
    )
    with pytest.raises(NotImplementedError):
        _ = sink_simulator.create(
            num=1,
            model=SinkModel.__name__,
        )


def test_sink_simulator_wrong_number(sink_simulator):
    with pytest.raises(NotImplementedError):
        _ = sink_simulator.create(
            num=2,
            model=SinkModel.__name__,
        )


def test_sink_simulator_wrong_name(sink_simulator):
    with pytest.raises(NotImplementedError):
        _ = sink_simulator.create(
            num=1,
            model=SinkModel.__name__ + '_',
        )


def test_sink_simulator_model_params(sink_simulator):
    with pytest.raises(NotImplementedError):
        _ = sink_simulator.create(
            num=1,
            model=SinkModel.__name__ + '_',
            model_params={'model_param_0': 1},
        )


def test_sink_simulator_step(sink_simulator):
    sink_simulator.step(
        time=0,
        inputs={'sink_model-0': {'my_attribute': 0}}
    )


def test_sink_simulator_step_and_get_data(sink_simulator):
    sink_simulator.step(
        time=0,
        inputs={'sink_model-0': {'my_attribute': 0}}
    )
    data = sink_simulator.get_data(
        outputs={'sink_model-0': {'my_attribute'}}
    )

    assert data is not None
    assert isinstance(data, dict)
    assert data == {}


def test_sink_simulator_create_step_and_get_data(sink_simulator):
    _ = sink_simulator.create(
        num=1,
        model=SinkModel.__name__,
    )
    sink_simulator.step(
        time=0,
        inputs={'SinkModel-0': {'my_attribute': 0}}
    )
    data = sink_simulator.get_data(
        outputs={'SinkModel-0': {'my_attribute'}}
    )

    assert data is not None
    assert isinstance(data, dict)
    assert data != {}

    assert 'SinkModel-0' in data.keys()
