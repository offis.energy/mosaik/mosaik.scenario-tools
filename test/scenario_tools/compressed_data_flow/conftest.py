from pytest import fixture

from mosaik_scenario_tools.scenario_tools.compressed_data_flow.\
    deflate_simulator import DeflateSimulator
from mosaik_scenario_tools.scenario_tools.compressed_data_flow.\
    inflate_simulator import InflateSimulator
from mosaik_scenario_tools.scenario_tools.compressed_data_flow.\
    inflate_sink_model import InflateSinkModel


@fixture(name='deflate_simulator')
def deflate_simulator_fixture() -> DeflateSimulator:
    deflate_simulator: DeflateSimulator = DeflateSimulator()

    return deflate_simulator


@fixture(name='inflate_simulator')
def inflate_simulator_fixture() -> InflateSimulator:
    inflate_simulator: InflateSimulator = InflateSimulator()

    return inflate_simulator


@fixture(name='inflate_sink_model')
def inflate_sink_model_fixture() -> InflateSinkModel:
    inflate_model: InflateSinkModel = InflateSinkModel(
        eid=InflateSinkModel.__name__ + '-0'
    )

    return inflate_model
