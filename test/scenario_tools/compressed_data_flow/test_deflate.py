from mosaik_scenario_tools.scenario_tools.compressed_data_flow.deflate_module \
    import deflate_data


def test_deflate_data_empty():
    # Mock
    data = {
    }

    # Test
    data_compressed = deflate_data(data=data)

    # Assert
    assert isinstance(data_compressed, dict)
    assert data_compressed == {}


def test_deflate_data_string():
    # Mock
    data = {
        0: 'foobar'
    }

    # Test
    data_compressed = deflate_data(data=data)

    # Assert
    assert isinstance(data_compressed, dict)
    assert data_compressed == {0: 'eAFTSsvPT0osUgIADFgCvg=='}


def test_deflate_data_int():
    # Mock
    data = {
        0: 42
    }

    # Test
    data_compressed = deflate_data(data=data)

    # Assert
    assert isinstance(data_compressed, dict)
    assert data_compressed == {0: 'eAEzMQIAAJwAZw=='}
