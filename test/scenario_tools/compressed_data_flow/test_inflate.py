from mosaik_scenario_tools.scenario_tools.compressed_data_flow.inflate_module \
    import inflate


def test_inflate():
    # Mock
    not_actually_compressed_byte_string: dict = {'foo': 'bar'}

    # Test
    data = inflate(compressed_byte_string=not_actually_compressed_byte_string)

    # Assert
    assert data
    assert isinstance(data, dict)
    assert data == not_actually_compressed_byte_string
