from pytest import raises

from mosaik_scenario_tools.scenario_tools.compressed_data_flow.\
    deflate_simulator import DeflateSimulator
from mosaik_scenario_tools.scenario_tools.compressed_data_flow.\
    deflate_source_model import DeflateSourceModel


def test_deflate_simulator_instantiation(deflate_simulator):
    assert deflate_simulator
    assert isinstance(deflate_simulator, DeflateSimulator)


def test_deflate_simulator_create_empty(deflate_simulator):
    list_of_model_type_to_eid = deflate_simulator.create(
        # empty
    )

    assert list_of_model_type_to_eid
    assert isinstance(list_of_model_type_to_eid, list)
    for model_type_and_eid in list_of_model_type_to_eid:
        assert isinstance(model_type_and_eid['type'], str)
        assert model_type_and_eid['type'] == DeflateSourceModel.__name__
        assert isinstance(model_type_and_eid['eid'], str)
        assert \
            model_type_and_eid['eid'] == \
            DeflateSourceModel.__name__ + '-0'


def test_deflate_simulator_create_wrong_num(deflate_simulator):
    with raises(NotImplementedError):
        _ = deflate_simulator.create(
            num=2,
        )


def test_deflate_simulator_create_wrong_model_type(deflate_simulator):
    with raises(NotImplementedError):
        _ = deflate_simulator.create(
            model='Foo',
        )


def test_deflate_simulator_create_wrong_model_params(deflate_simulator):
    with raises(NotImplementedError):
        _ = deflate_simulator.create(
            model_params={'Foo': 'Bar'},
        )


def test_deflate_simulator_step_wrong_inputs(deflate_simulator):
    with raises(NotImplementedError):
        _ = deflate_simulator.step(
            time=0,
            inputs={'Foo': 'Bar'},
        )


def test_deflate_simulator_get_data_wrong_model_name(deflate_simulator):
    _ = deflate_simulator.create()

    with raises(NotImplementedError):
        _ = deflate_simulator.get_data(
            outputs={
                'Foo': 'Bar',
            },
        )


def test_deflate_simulator_get_data_other_model_name(deflate_simulator):
    _ = deflate_simulator.create()

    with raises(NotImplementedError):
        _ = deflate_simulator.get_data(
            outputs={
                DeflateSourceModel.__name__ + '-0': 'Bar',
                'Bar': 'Foo',
            },
        )
