from pytest import raises


def test_inflate_model(inflate_sink_model):
    # Mock
    data: dict = {
        'float': {
            'DeflateSimulator-0.DeflateSourceModel-0': [3.14],
        },
        'bool': {
            'DeflateSimulator-0.DeflateSourceModel-0': [True],
        },
        'str': {
            'DeflateSimulator-0.DeflateSourceModel-0': ['Foo'],
        },
        'None': {
            'DeflateSimulator-0.DeflateSourceModel-0': [None],
        },
        'int': {
            'DeflateSimulator-0.DeflateSourceModel-0': [42],
        },
        'dict': {
            'DeflateSimulator-0.DeflateSourceModel-0': [{}],
        },
    }

    # Test
    inflate_sink_model.step(data=data)

    # Assert
    assert True


def test_inflate_model_step_wrong_float_type(inflate_sink_model):
    # Mock
    data: dict = {
        'float': {
            'DeflateSimulator-0.DeflateSourceModel-0': [True],
        },
        'bool': {
            'DeflateSimulator-0.DeflateSourceModel-0': [True],
        },
        'str': {
            'DeflateSimulator-0.DeflateSourceModel-0': ['Foo'],
        },
        'None': {
            'DeflateSimulator-0.DeflateSourceModel-0': [None],
        },
        'int': {
            'DeflateSimulator-0.DeflateSourceModel-0': [42],
        },
        'dict': {
            'DeflateSimulator-0.DeflateSourceModel-0': [{}],
        },
    }

    # Test
    with raises(TypeError):
        inflate_sink_model.step(data=data)

    # Assert
    assert True


def test_inflate_model_step_wrong_bool_type(inflate_sink_model):
    # Mock
    data: dict = {
        'float': {
            'DeflateSimulator-0.DeflateSourceModel-0': [3.14],
        },
        'bool': {
            'DeflateSimulator-0.DeflateSourceModel-0': ['True'],
        },
        'str': {
            'DeflateSimulator-0.DeflateSourceModel-0': ['Foo'],
        },
        'None': {
            'DeflateSimulator-0.DeflateSourceModel-0': [None],
        },
        'int': {
            'DeflateSimulator-0.DeflateSourceModel-0': [42],
        },
        'dict': {
            'DeflateSimulator-0.DeflateSourceModel-0': [{}],
        },
    }

    # Test
    with raises(TypeError):
        inflate_sink_model.step(data=data)

    # Assert
    assert True


def test_inflate_model_step_wrong_str_type(inflate_sink_model):
    # Mock
    data: dict = {
        'float': {
            'DeflateSimulator-0.DeflateSourceModel-0': [3.14],
        },
        'bool': {
            'DeflateSimulator-0.DeflateSourceModel-0': [True],
        },
        'str': {
            'DeflateSimulator-0.DeflateSourceModel-0': [True],
        },
        'None': {
            'DeflateSimulator-0.DeflateSourceModel-0': [None],
        },
        'int': {
            'DeflateSimulator-0.DeflateSourceModel-0': [42],
        },
        'dict': {
            'DeflateSimulator-0.DeflateSourceModel-0': [{}],
        },
    }

    # Test
    with raises(TypeError):
        inflate_sink_model.step(data=data)

    # Assert
    assert True


def test_inflate_model_step_wrong_none_type(inflate_sink_model):
    # Mock
    data: dict = {
        'float': {
            'DeflateSimulator-0.DeflateSourceModel-0': [3.14],
        },
        'bool': {
            'DeflateSimulator-0.DeflateSourceModel-0': [True],
        },
        'str': {
            'DeflateSimulator-0.DeflateSourceModel-0': ['Foo'],
        },
        'None': {
            'DeflateSimulator-0.DeflateSourceModel-0': ['None'],
        },
        'int': {
            'DeflateSimulator-0.DeflateSourceModel-0': [42],
        },
        'dict': {
            'DeflateSimulator-0.DeflateSourceModel-0': [{}],
        },
    }

    # Test
    with raises(TypeError):
        inflate_sink_model.step(data=data)

    # Assert
    assert True


def test_inflate_model_step_wrong_int_type(inflate_sink_model):
    # Mock
    data: dict = {
        'float': {
            'DeflateSimulator-0.DeflateSourceModel-0': [3.14],
        },
        'bool': {
            'DeflateSimulator-0.DeflateSourceModel-0': [True],
        },
        'str': {
            'DeflateSimulator-0.DeflateSourceModel-0': ['Foo'],
        },
        'None': {
            'DeflateSimulator-0.DeflateSourceModel-0': [None],
        },
        'int': {
            'DeflateSimulator-0.DeflateSourceModel-0': ['42'],
        },
        'dict': {
            'DeflateSimulator-0.DeflateSourceModel-0': [{}],
        },
    }

    # Test
    with raises(TypeError):
        inflate_sink_model.step(data=data)

    # Assert
    assert True


def test_inflate_model_step_wrong_dict_type(inflate_sink_model):
    # Mock
    data: dict = {
        'float': {
            'DeflateSimulator-0.DeflateSourceModel-0': [3.14],
        },
        'bool': {
            'DeflateSimulator-0.DeflateSourceModel-0': [True],
        },
        'str': {
            'DeflateSimulator-0.DeflateSourceModel-0': ['Foo'],
        },
        'None': {
            'DeflateSimulator-0.DeflateSourceModel-0': [None],
        },
        'int': {
            'DeflateSimulator-0.DeflateSourceModel-0': [42],
        },
        'dict': {
            'DeflateSimulator-0.DeflateSourceModel-0': ['{}'],
        },
    }

    # Test
    with raises(TypeError):
        inflate_sink_model.step(data=data)

    # Assert
    assert True


def test_inflate_model_step_wrong_float_list(inflate_sink_model):
    # Mock
    data: dict = {
        'float': {
            'DeflateSimulator-0.DeflateSourceModel-0': 3.14,
        },
        'bool': {
            'DeflateSimulator-0.DeflateSourceModel-0': [True],
        },
        'str': {
            'DeflateSimulator-0.DeflateSourceModel-0': ['Foo'],
        },
        'None': {
            'DeflateSimulator-0.DeflateSourceModel-0': [None],
        },
        'int': {
            'DeflateSimulator-0.DeflateSourceModel-0': [42],
        },
        'dict': {
            'DeflateSimulator-0.DeflateSourceModel-0': [{}],
        },
    }

    # Test
    with raises(TypeError):
        inflate_sink_model.step(data=data)

    # Assert
    assert True


def test_inflate_model_step_wrong_bool_list(inflate_sink_model):
    # Mock
    data: dict = {
        'float': {
            'DeflateSimulator-0.DeflateSourceModel-0': [3.14],
        },
        'bool': {
            'DeflateSimulator-0.DeflateSourceModel-0': True,
        },
        'str': {
            'DeflateSimulator-0.DeflateSourceModel-0': ['Foo'],
        },
        'None': {
            'DeflateSimulator-0.DeflateSourceModel-0': [None],
        },
        'int': {
            'DeflateSimulator-0.DeflateSourceModel-0': [42],
        },
        'dict': {
            'DeflateSimulator-0.DeflateSourceModel-0': [{}],
        },
    }

    # Test
    with raises(TypeError):
        inflate_sink_model.step(data=data)

    # Assert
    assert True


def test_inflate_model_step_wrong_str_list(inflate_sink_model):
    # Mock
    data: dict = {
        'float': {
            'DeflateSimulator-0.DeflateSourceModel-0': [3.14],
        },
        'bool': {
            'DeflateSimulator-0.DeflateSourceModel-0': [True],
        },
        'str': {
            'DeflateSimulator-0.DeflateSourceModel-0': 'Foo',
        },
        'None': {
            'DeflateSimulator-0.DeflateSourceModel-0': [None],
        },
        'int': {
            'DeflateSimulator-0.DeflateSourceModel-0': [42],
        },
        'dict': {
            'DeflateSimulator-0.DeflateSourceModel-0': [{}],
        },
    }

    # Test
    with raises(TypeError):
        inflate_sink_model.step(data=data)

    # Assert
    assert True


def test_inflate_model_step_wrong_none_list(inflate_sink_model):
    # Mock
    data: dict = {
        'float': {
            'DeflateSimulator-0.DeflateSourceModel-0': [3.14],
        },
        'bool': {
            'DeflateSimulator-0.DeflateSourceModel-0': [True],
        },
        'str': {
            'DeflateSimulator-0.DeflateSourceModel-0': ['Foo'],
        },
        'None': {
            'DeflateSimulator-0.DeflateSourceModel-0': None,
        },
        'int': {
            'DeflateSimulator-0.DeflateSourceModel-0': [42],
        },
        'dict': {
            'DeflateSimulator-0.DeflateSourceModel-0': [{}],
        },
    }

    # Test
    with raises(TypeError):
        inflate_sink_model.step(data=data)

    # Assert
    assert True


def test_inflate_model_step_wrong_int_list(inflate_sink_model):
    # Mock
    data: dict = {
        'float': {
            'DeflateSimulator-0.DeflateSourceModel-0': [3.14],
        },
        'bool': {
            'DeflateSimulator-0.DeflateSourceModel-0': [True],
        },
        'str': {
            'DeflateSimulator-0.DeflateSourceModel-0': ['Foo'],
        },
        'None': {
            'DeflateSimulator-0.DeflateSourceModel-0': [None],
        },
        'int': {
            'DeflateSimulator-0.DeflateSourceModel-0': 42,
        },
        'dict': {
            'DeflateSimulator-0.DeflateSourceModel-0': [{}],
        },
    }

    # Test
    with raises(TypeError):
        inflate_sink_model.step(data=data)

    # Assert
    assert True


def test_inflate_model_step_wrong_dict_list(inflate_sink_model):
    # Mock
    data: dict = {
        'float': {
            'DeflateSimulator-0.DeflateSourceModel-0': [3.14],
        },
        'bool': {
            'DeflateSimulator-0.DeflateSourceModel-0': [True],
        },
        'str': {
            'DeflateSimulator-0.DeflateSourceModel-0': ['Foo'],
        },
        'None': {
            'DeflateSimulator-0.DeflateSourceModel-0': [None],
        },
        'int': {
            'DeflateSimulator-0.DeflateSourceModel-0': [42],
        },
        'dict': {
            'DeflateSimulator-0.DeflateSourceModel-0': {},
        },
    }

    # Test
    with raises(TypeError):
        inflate_sink_model.step(data=data)

    # Assert
    assert True


def test_inflate_model_get_data(inflate_sink_model):
    # Test
    with raises(NotImplementedError):
        inflate_sink_model.get_data()

    # Assert
    assert True
