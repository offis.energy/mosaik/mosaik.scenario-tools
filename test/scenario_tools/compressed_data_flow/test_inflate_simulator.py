from pytest import raises

from mosaik_scenario_tools.scenario_tools.compressed_data_flow.\
    inflate_simulator import InflateSimulator
from mosaik_scenario_tools.scenario_tools.compressed_data_flow.\
    inflate_sink_model import InflateSinkModel


def test_inflate_simulator_instantiation(inflate_simulator):
    assert inflate_simulator
    assert isinstance(inflate_simulator, InflateSimulator)


def test_inflate_simulator_create_empty(inflate_simulator):
    list_of_model_type_to_eid = inflate_simulator.create(
        # empty
    )

    assert list_of_model_type_to_eid
    assert isinstance(list_of_model_type_to_eid, list)
    for model_type_and_eid in list_of_model_type_to_eid:
        assert isinstance(model_type_and_eid['type'], str)
        assert model_type_and_eid['type'] == InflateSinkModel.__name__
        assert isinstance(model_type_and_eid['eid'], str)
        assert model_type_and_eid['eid'] == InflateSinkModel.__name__ + '-0'


def test_inflate_simulator_create_wrong_num(inflate_simulator):
    with raises(NotImplementedError):
        _ = inflate_simulator.create(
            num=2,
        )


def test_inflate_simulator_create_wrong_model_type(inflate_simulator):
    with raises(NotImplementedError):
        _ = inflate_simulator.create(
            model='Foo',
        )


def test_inflate_simulator_create_wrong_model_params(inflate_simulator):
    with raises(NotImplementedError):
        _ = inflate_simulator.create(
            model_params={'Foo': 'Bar'},
        )


def test_inflate_simulator_step_no_inputs(inflate_simulator):
    with raises(NotImplementedError):
        _ = inflate_simulator.step(
            time=0,
            inputs={},
        )


def test_inflate_simulator_step_wrong_inputs(inflate_simulator):
    with raises(NotImplementedError):
        _ = inflate_simulator.step(
            time=0,
            inputs={'Foo': 'Bar'},
        )


def test_inflate_simulator_step_too_many_inputs(inflate_simulator):
    with raises(NotImplementedError):
        _ = inflate_simulator.step(
            time=0,
            inputs={
                'Foo': 'Bar',
                InflateSinkModel.__name__ + '-0': 'FooBar',
            },
        )


def test_inflate_simulator_get_data_wrong_model_name(inflate_simulator):
    _ = inflate_simulator.create()

    with raises(NotImplementedError):
        _ = inflate_simulator.get_data(
            outputs={
                'Foo': 'Bar',
            },
        )


def test_inflate_simulator_get_data_other_model_name(inflate_simulator):
    _ = inflate_simulator.create()

    with raises(NotImplementedError):
        _ = inflate_simulator.get_data(
            outputs={
                InflateSinkModel.__name__ + '-0': 'Bar',
                'Bar': 'Foo',
            },
        )


def test_inflate_simulator_get_data_return(inflate_simulator):
    _ = inflate_simulator.create()

    data = inflate_simulator.get_data(
        outputs={
            InflateSinkModel.__name__ + '-0': 'Bar',
        },
    )

    assert data == {}
