#   mosaik.Scenario-Tools

Tools for building mosaik scenarios.

## Status

[![pipeline status](https://gitlab.com/offis.energy/mosaik/mosaik.scenario-tools/badges/master/pipeline.svg)](https://gitlab.com/offis.energy/mosaik/mosaik.scenario-tools/pipelines)
[![coverage report](https://gitlab.com/offis.energy/mosaik/mosaik.scenario-tools/badges/master/coverage.svg)](https://gitlab.com/offis.energy/mosaik/mosaik.scenario-tools/-/jobs)
[![libraries status](https://img.shields.io/librariesio/release/pypi/mosaik.scenario-tools)](https://libraries.io/pypi/mosaik.scenario-tools)
[![license badge](https://img.shields.io/pypi/l/mosaik.Scenario-Tools)](#)
[![PyPI version](https://img.shields.io/pypi/v/mosaik.Scenario-Tools)](https://pypi.org/project/mosaik.Scenario-Tools/#history)
[![Python Versions](https://img.shields.io/pypi/pyversions/mosaik.Scenario-Tools)](https://pypi.org/project/mosaik.Scenario-Tools)

##  Prerequisites

Under Windows:

-   install Python 3.7 64 bit for all users

Under Ubuntu

    apt install python3.7

## Ensuring pip

    python -m ensurepip

##  Creating the Virtual Environment

Under Linux

    python3.8 -m venv venv

Under Windows

    "C:\Program Files\Python38\python.exe" -m venv venv

##  Installing tox

    python -m pip install --upgrade -r requirements.d/venv.txt

## Installing Runtime Requirements

    python -m pip install --upgrade -r requirements.d/base.txt

## Testing

    pytest

## Freezing the Virtual Environment Requirements

Under Linux

     venv/bin/python -m pip uninstall pkg_resources
     venv/bin/python -m pip freeze --all --exclude-editable > requirements.d/venv.txt
     # And remove line beginning with package name

Under Windows

     venv\Scripts\python.exe -m pip freeze --all --exclude-editable > requirements.d/venv.txt
    # And remove line beginning with package name

## Freezing the Tox Requirements

Under Linux

    .tox/py36/bin/python -m pip uninstall pkg_resources
    .tox/py36/bin/python -m pip freeze --all --exclude-editable > requirements.d/base.txt
    # And remove line beginning with package name

Under Windows

    .tox\py36\Scripts\python.exe -m pip freeze --all --exclude-editable > requirements.d\base.txt
    # And remove line beginning with package name
